const sharp = require('sharp');
const ffmpeg = require('fluent-ffmpeg');

/**
 * Create a scaled thumbnail
 * @param {string} topic            Post topic
 * @param {string} username         Poster username
 * @returns {Promise} Resolves when done
 */
function createThumbnail(filepath, ext, postID){
    return new Promise((resolve, reject)=>{
        if(['webm', 'mp4'].includes(ext)){
            ffmpeg(filepath).outputOptions('-vframes 1').videoFilters('scale=256:128:force_original_aspect_ratio=decrease').output('thumbs/' + postID + '.webp').on('end', ()=>{
               resolve();
            }).on('error', function(err, stdout, stderr){
                reject(err);
            }).run();
        }else{
            sharp(filepath).rotate().resize(256, 128, {fit: 'inside'})
            .toFile('thumbs/' + postID + '.webp').then(()=>{
                resolve();
            }).catch(err => {
                reject(err);
            });
        }
    });
}

module.exports = createThumbnail;
