const sqlite = require('sqlite');
const sqlite3 = require('sqlite3');

class CanvasDB {
    constructor(filename) {
        this.filename = filename;
    }

    /**
     * Initialize the database
     */
    async init() {
        this.db = await sqlite.open({
            driver: sqlite3.Database,
            filename: this.filename
        });
        await this.db.exec('CREATE TABLE IF NOT EXISTS b (id INTEGER PRIMARY KEY ASC, parent INTEGER, datetime INTEGER, lastactive INTEGER, ipaddress TEXT, x INTEGER, y INTEGER, topic TEXT, username TEXT, content TEXT, original_filename TEXT, filename TEXT, replies INTEGER);');
    }

    /**
     * Get posts from the database
     * @param {number|null} parent Parent post ID
     * @param {number} x1          Top left X
     * @param {number} y1          Top left Y
     * @param {number} x2          Bottom right X
     * @param {number} y2          Bottom right Y
     * @param {Array<number>} have Array of post IDs that the client already has
     * @param {number} limit       Limit number of results (0 if unlimited)
     */
    async getPosts(parent, x1, y1, x2, y2, have, limit) {
        const verifiedHave = [];
        if(have) have.forEach(item=>{
            if(isNaN(item)) return;
            verifiedHave.push(Number(item));
        });
        if(parent){
            const parentPost = await this.db.get(`SELECT parent FROM b WHERE id IS ?;`, parent);
            if(parentPost) if(parentPost.parent != null) return parentPost.parent;
        }
        const haveIDs = verifiedHave.join(',');
        const result = await this.db.all(
            `SELECT * FROM (
                SELECT id, parent, datetime, lastactive, x, y, topic, username, content, original_filename, filename, replies FROM b
                    WHERE (id IS ?) OR
                    (parent IS ? AND x BETWEEN ? AND ? AND y BETWEEN ? AND ?)
                    ORDER BY datetime DESC${(parent && limit > 0) ? '' :  ' LIMIT ' + limit}
            ) WHERE id NOT IN (${haveIDs});`,
            parent, parent,
            x1,
            x2,
            y1,
            y2
        );
        if(result.length == 0) return null;
        if(parent){
            for(let post of result){
                if(post.parent == null){
                    post.x = post.y = 0;
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Select posts and their replies
     * @param {Array<number>} ids Post IDs
     */
    async selectPosts(ids){
        const placeholders = new Array(ids.length).fill('?').join(',');
        return await this.db.all(`SELECT * FROM b WHERE id IN (${placeholders}) OR parent IN (${placeholders});`, ...ids, ...ids);
    }

    /**
     * Insert a post
     * @param {number|null} parent      Parent post ID
     * @param {string|null} ipaddress   Poster's IP or null to bypass the post cooldown
     * @param {number} x                X coordinate
     * @param {number} y                Y coordinate
     * @param {string} topic            Post topic
     * @param {string} username         Poster username
     * @param {string} content          Post content
     * @param {string} originalFilename The original filename
     * @param {string} filename         Filename on server
     * @returns {number}                Post ID
     */
    async insertPost(parent, ipaddress, x, y, topic, username, content, originalFilename, filename){
        const dateNow = Date.now();
        const max = await this.db.get('SELECT MAX(id) FROM b;');
        if(ipaddress){
            const lastdate = await this.db.get(`SELECT datetime FROM b WHERE ipaddress=? ORDER BY id DESC LIMIT 1;`, ipaddress);
            if(lastdate) if((lastdate.datetime + 60000) > dateNow) return -1;
        }
        let parentPost;
        if(parent){
            parentPost = await this.db.get(`SELECT parent, id FROM b WHERE id IS ?;`, parent);
            if(!parentPost) return -2;
            if(parentPost.parent != null) return -3;
        }   

        const id = max['MAX(id)'] + 1;
        const datetime = Date.now();
        await this.db.run(
            'INSERT INTO b VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);',
            id,
            parent,
            datetime,
            datetime,
            ipaddress,
            x,
            y,
            topic,
            username,
            content,
            originalFilename,
            filename,
            0
        );
        if(parentPost) await this.db.run(
            'UPDATE b SET replies = (SELECT COUNT(id) FROM b WHERE parent = ?), lastactive = ? WHERE id IS ?;',
            parentPost.id, datetime, parentPost.id
        );
        return id;
    }

    /**
     * Delete posts and their replies
     * @param {Array<number>} ids Post IDs
     */
    async deletePosts(ids){
        const placeholders = new Array(ids.length).fill('?').join(',');
        await this.db.run(`DELETE FROM b WHERE id IN (${placeholders}) OR parent IN (${placeholders});`, ...ids, ...ids);
        await this.db.run('UPDATE b AS board SET replies = (SELECT COUNT(*) FROM b WHERE b.parent = board.id);');
    }
}

module.exports = CanvasDB;