const canvas = document.getElementById('canvas');
const postDialog = document.getElementById('post');
const hint = document.getElementById('hint');
const postClose = document.getElementById('close');
const formParent = document.getElementById('formParent');
const formX = document.getElementById('formX');
const formY = document.getElementById('formY');

postClose.onclick = function(){
    postDialog.style.display = 'none';
}

// canvas.oncontextmenu = ()=>{return false;};

const threadID = new URL(document.location).searchParams.get('t');
formParent.value = threadID;

let threads = [];

let lastZIndex = 0;

const pos = {
    x: 0,
    y: 0
}

const canvasPos = {
    x: 0,
    y: 0
}

const movingPost = {
    post: null,
    moved: false,
    pageX: 0,
    pageY: 0,
    origX: 0,
    origY: 0
}

postDialog.onsubmit = function(){
    formX.value = pos.x + canvasPos.x;
    formY.value = pos.y + canvasPos.y;
    return true;
}

const drag = {
    dragging: false,
    screenStartX: 0,
    screenStartY: 0,
    screenDX: 0,
    screenDY: 0,
    touchTimeout: 0
}

function moveThreads(){
    const tempX = pos.x - drag.screenDX;
    const tempY = pos.y - drag.screenDY;
    for(let thread of threads){
        thread.thread.style.left = thread.x - tempX + 'px';
        thread.thread.style.top = thread.y - tempY + 'px';
    }
    hint.innerHTML = `x: ${tempX}<br>y: ${tempY}`;
}

function moveThreadsPos(){
    for(let thread of threads){
        thread.thread.style.left = thread.x - pos.x + 'px';
        thread.thread.style.top = thread.y - pos.y + 'px';
    }
    hint.innerHTML = `x: ${pos.x}<br>y: ${pos.y}`;
}

document.onpointerdown = function(event){
    if((event.pointerType == 'mouse' && event.button == 1) || event.pointerType == 'touch'){
        drag.dragging = true;
        drag.screenStartX = event.pageX;
        drag.screenStartY = event.pageY;
        if(event.pointerType == 'touch'){
            clearTimeout(drag.touchTimeout);
            drag.touchTimeout = setTimeout(function(){
                if(drag.screenDX > 10 || drag.screenDY > 10) return;
                drag.dragging = false;
                postDialog.style.display = '';
                postDialog.style.left = event.pageX + 'px';
                postDialog.style.top = event.pageY + 'px';
            }, 500);
        }
    }
}

document.onpointermove = function(event){
    if(drag.dragging){
        drag.screenDX = event.pageX - drag.screenStartX;
        drag.screenDY = event.pageY - drag.screenStartY;
        moveThreads();
    }
    if(movingPost.post){
        pageDX = event.pageX - movingPost.pageX;
        pageDY = event.pageY - movingPost.pageY;
        movingPost.moved = true;
        movingPost.post.x = movingPost.origX + pageDX;
        movingPost.post.y = movingPost.origY + pageDY;
        movingPost.post.thread.style.left = movingPost.post.x - pos.x + 'px';
        movingPost.post.thread.style.top = movingPost.post.y - pos.y + 'px';
    }
}

document.onpointerup = function(event){
    if((event.pointerType == 'mouse' && event.button == 1) || event.pointerType == 'touch'){
        if(!drag.dragging) return;
        drag.dragging = false;
        pos.x = pos.x - drag.screenDX;
        pos.y = pos.y - drag.screenDY;
        updatePosition();
        if(event.pointerType == 'touch') clearTimeout(drag.touchTimeout);
    }
    movingPost.post = null;
    setTimeout(function(){
        movingPost.moved = false;
    }, 1);
}

let lastKeyUpdate = 0;
document.onkeydown = function(event){
    if(event.code == 'Escape' && overlay){
        overlay.remove();
        overlay = null;
    }
    if(event.target != document.body) return;
    const delta = 50;
    switch(event.code){
        case 'ArrowLeft':
            pos.x -= delta;
            break;
        case 'ArrowRight':
            pos.x += delta;
            break;
        case 'ArrowUp':
            pos.y -= delta;
            break;
        case 'ArrowDown':
            pos.y += delta;
            break;
    }
    moveThreadsPos();
    if(Date.now() - lastKeyUpdate < 1000) return;
    lastKeyUpdate = Date.now();
    updatePosition();
}

canvas.onwheel = function(event){
    if(!event.getModifierState('CapsLock')) return;
    if (event.shiftKey && event.deltaX === 0) {
        // Holding down shift will allow pos.x movement for traditional scrollwheels that only change event.deltaY.
        pos.x += event.deltaY;
    } else {
        // touchpad or scrollwheel movement
        pos.x += event.deltaX;
        pos.y += event.deltaY;
    }
    moveThreadsPos();
    if(Date.now() - lastKeyUpdate < 1000) return;
    lastKeyUpdate = Date.now();
    updatePosition();
};

canvas.onclick = function(event){
    if(event.button != 0) return;
    if(event.detail != 2) return;
    postDialog.style.display = '';
    postDialog.style.left = event.pageX + 'px';
    postDialog.style.top = event.pageY + 'px';
    canvasPos.x = event.pageX;
    canvasPos.y = event.pageY;
}

let overlay;
function previewFile(url){
    let ext = url.split('.').pop();
    let viewElement;
    if(['webm', 'mp4'].includes(ext)){
        viewElement = document.createElement('video');
        viewElement.loop = viewElement.controls = true;
    }else{
        viewElement = document.createElement('img');
    }
    viewElement.className = 'preview';
    viewElement.src = url;

    overlay = document.createElement('div');
    overlay.className = 'overlay';
    overlay.appendChild(viewElement);
    overlay.onclick = function(){
        overlay.remove();
        overlay = null;
    }
    document.body.appendChild(overlay);
}

function insertThread(parent, id, headerDiv, text, image, x, y, time){
    const thread = document.createElement('div');
    thread.onclick = function(){
        lastZIndex++;
        this.style.zIndex = lastZIndex;
    }
    thread.style.zIndex = time;
    if(time > lastZIndex) lastZIndex = time;
    thread.className = parent ? 'reply' : 'thread';

    const postContent = document.createElement('div');
    postContent.className = 'postContent';

    headerDiv.className = 'postHeader';
    const collapseButton = document.createElement('a');
    collapseButton.innerHTML = '&#9660;';
    collapseButton.className = 'collapse';
    let collapsed = false;
    collapseButton.onclick = function(){
        collapsed = !collapsed;
        this.innerHTML = collapsed ? '&#9650;' : '&#9660;';
        postContent.style.display = collapsed ? 'none' : '';
    }
    headerDiv.appendChild(collapseButton);
    thread.appendChild(headerDiv);

    if(image){
        const img = document.createElement('img');
        img.src = 'thumbs/' + id + '.webp';
        img.className = 'threadThumb';
        img.onclick = function(){
            previewFile('/images/' + image);
        }
        postContent.appendChild(img);
    }
    

    postContent.insertAdjacentHTML('beforeend', text);

    thread.appendChild(postContent);

    canvas.appendChild(thread);
    thread.style.left = x - pos.x + 'px';
    thread.style.top = y - pos.y + 'px';
    const threadObj = {
        id, x, y, thread
    }
    headerDiv.onpointerdown = function(event){
        event.preventDefault();
        movingPost.post = threadObj;
        movingPost.pageX = event.pageX;
        movingPost.pageY = event.pageY;
        movingPost.origX = threadObj.x;
        movingPost.origY = threadObj.y;
        lastZIndex++;
        thread.style.zIndex = lastZIndex;
    }
    threads.push(threadObj);
}

let socketURL;
if(window.location.protocol == 'https:'){
    socketURL = 'wss://' + window.location.host;
}else{
    socketURL = 'ws://' + window.location.host;
}

const socket = new WebSocket(socketURL);

let updateTimeout = 0;
function updatePosition(){
    clearTimeout(updateTimeout);
    const w = document.documentElement.clientWidth;
    const h = document.documentElement.clientHeight
    threads = threads.filter(thread=>{
        if(thread.x < pos.x - 2 * w || thread.x > pos.x + 3 * w || thread.y < pos.y - 2 * h || thread.y > pos.y + 3 * h){
            thread.thread.remove();
            return false;
        }
        return true;
    });
    updateTimeout = setTimeout(updatePosition, 10000);
    if(socket.readyState != 1) return;
    socket.send(JSON.stringify({
        t: 'p',
        x: pos.x,
        y: pos.y,
        w,
        h,
        p: threadID,
        have: threads.map(val=>val.id)
    }));
}

socket.onopen = function(){
    updatePosition();
}

const escaper = document.createElement('label');
// const URLRegex = /(?<=^|\s)((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/g;
const URLRegex = new RegExp('(^|\\s)((([A-Za-z]{3,9}:(?:\\/\\/)?)(?:[\\-;:&=\\+\\$,\\w]+@)?[A-Za-z0-9\\.\\-]+|(?:www\\.|[\\-;:&=\\+\\$,\\w]+@)[A-Za-z0-9\\.\\-]+)((?:\\/[\\+~%\\/\\.\\w\\-_]*)?\\??(?:[\\-\\+=&;%@\\.\\w_]*)#?(?:[\\.\\!\\/\\\\\\w]*))?)', 'g');

function formatPost(text){
    escaper.textContent = text;
    let escaped = escaper.innerHTML;
    escaped = escaped.replace(/^&gt;.*/gm, substr => `<span class="greenText">${substr}</span>`);
    // escaped = escaped.replace(URLRegex, substr => `<a rel="noreferrer" target="_blank" href="${substr}">${substr}</a>`);
    escaped = escaped.replace(URLRegex, (__, _, str) => `${_}<a rel="noreferrer" target="_blank" href="${str}">${str}</a>`);
    return escaped.replaceAll('\n', '<br>');
}

function createThreads(toParse){
    for(let thread of toParse){
        if(threads.find(existingThread=>existingThread.id == thread.id) != undefined) return;

        let header = document.createElement('div');

        let usernameLabel = document.createElement('label');
        usernameLabel.className = 'username';
        usernameLabel.textContent = thread.username;
        header.appendChild(usernameLabel);
        header.insertAdjacentHTML('beforeend', ' ');

        let topicLabel = document.createElement('label');
        topicLabel.className = 'topic';
        topicLabel.textContent = thread.topic;
        header.appendChild(topicLabel);
        header.insertAdjacentHTML('beforeend', ' ');

        let dateRepliesLabel = document.createElement('label');
        dateRepliesLabel.textContent = new Date(thread.datetime).toLocaleString('en-GB') + ' Replies: ' + thread.replies;
        header.appendChild(dateRepliesLabel);
        header.insertAdjacentHTML('beforeend', ' ');

        let threadIDLink = document.createElement('a');
        threadIDLink.textContent = 'No.' + thread.id;
        threadIDLink.href = '/?t=' + thread.id;
        threadIDLink.onclick = function(event){
            if(movingPost.moved) event.preventDefault();
        }
        header.appendChild(threadIDLink);
        header.insertAdjacentHTML('beforeend', '<br>');

        let downloadLink = document.createElement('a');
        downloadLink.download = thread.original_filename;
        downloadLink.textContent = thread.original_filename;
        downloadLink.target = '_blank';
        downloadLink.href = '/images/' + thread.filename;
        downloadLink.onclick = function(event){
            if(movingPost.moved) event.preventDefault();
        }
        header.appendChild(downloadLink);

        insertThread(
            thread.parent,
            thread.id,
            header,
            formatPost(thread.content),
            thread.filename,
            thread.x, thread.y,
            Math.floor(thread.lastactive / 1000)
        );

        if(thread.topic) if(threadID == thread.id && thread.parent == null && thread.topic.length > 0) document.title = thread.topic + ' - Canvas Imageboard';
    }
}

socket.onmessage = function(message){
    const parsed = JSON.parse(message.data);
    switch(parsed.t){
        // error
        case 'e':
            alert(parsed.err);
            break;
        // posts
        case 'p':
            createThreads(parsed.p);
            break;
        // redirect to proper parent
        case 'r':
            window.location.href = '/?t=' + parsed.p;
            break;
        default:
            break;
    }
}

const featureNumber = 1;
const tutorialState = localStorage.getItem('tutorial');
if(tutorialState < featureNumber){
    localStorage.setItem('tutorial', featureNumber);
    alert(
        (tutorialState < 1 ? 'It looks like this is your first time here.\n\n' : 'Some things changed since the last time you were here.\n\n') + 
        'Double-click to post.\n' +
        'Use middle-click or arrow keys to move.\n' +
        'Enable Caps Lock to move by scrolling (hold shift to move horizontally).\n' +
        'Click on a thread No. to visit its page and reply to it.\n\n'+
        'If you keep getting this same popup, make sure your localStorage is enabled and not being deleted.'
    );
}
