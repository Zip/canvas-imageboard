const express = require('express');
const cors = require('cors')
const ws = require('ws');
const fs = require('fs');
const fileUpload = require('express-fileupload');
const CanvasDB = require('./db.js');
const createThumbnail = require('./thumb.js');

const config = require('./config.json');
const bans = require('./bans.json');

const app = express();

const wsServer = new ws.Server({ noServer: true });
wsServer.on('error', err=>{
    console.warn(err);
});

let db;

wsServer.on('connection', socket=>{
    socket.on('message', async message=>{
        let parsed;
        try{
            parsed = JSON.parse(message);
        }catch(err){
            return;
        }
        switch(parsed.t){
            case 'p':
                if(isNaN(parsed.x) || isNaN(parsed.y) || isNaN(parsed.w) || isNaN(parsed.h)) return;
                if(parsed.w > 8192 || parsed.h > 8192){
                    socket.send(JSON.stringify({
                        t: 'e',
                        err: 'Screen too large'
                    }));
                    return;
                }
                const posts = await db.getPosts(parsed.p, parsed.x - parsed.w, parsed.y - parsed.h, parsed.x + 2 * parsed.w, parsed.y + 2 * parsed.h, parsed.have, config.limit);
                if(posts == null) return;
                socket.send(JSON.stringify({
                    t: isNaN(posts) ? 'p' : 'r',
                    p: posts
                }));
            default:
                break;
        }
    });
});

async function createPost(parent, ip, x, y, topic, username, content, originalFilename, filename){
    const id = db.insertPost(
        parent ? parent : null,
        ip, x, y,
        topic,
        username ? username : 'Anonymous',
        content,
        originalFilename,
        filename
    );
    return id;
}

async function removePosts(ids){
    const foundPosts = await db.selectPosts(ids);
    foundPosts.forEach(post=>{
        fs.rm('thumbs/' + post.id + '.webp', function(err){
            if(err) console.log(err);
        });
        fs.rm('images/' + post.filename, function(err){
            if(err) console.log(err);
        });
    });
    await db.deletePosts(ids);
}

async function handlePost(req, res){
    if(!req.body) return res.send('Malformed post request');
    for(let param of ['parent', 'x', 'y', 'topic', 'username', 'content']){
        if(typeof req.body[param] != 'string') return res.send('Malformed post request');
    }

    const parentID = Number(req.body.parent);

    if(parentID == 0 && req.files == null) return res.send('A new thread must contain a file');
    if(parentID == 0 && req.body.content == '') return res.send('A new thread must have text content');
    if(req.body.content == '' && req.files == null) return res.send('A reply must have text content or a file');

    if(req.body.content.length > 4000) return res.send('Your post cannot belonger than 4000 characters');
    if(req.body.topic.length > 64) return res.send('The topic cannot be longer than 64 characters');
    if(req.body.username.length > 32) return res.send('Your username cannot belonger than 32 characters');

    let newFilename = null;
    let originalFilename = null;
    let mvPath;
    let ext;
    if(req.files){
        file = req.files.file;
        originalFilename = req.files.file.name;
        if(!file.name.includes('.')) return res.send('File has no extension');
        ext = file.name.split('.').pop();
        if(!['webp', 'png', 'jpg', 'jpeg', 'gif', 'webm', 'mp4'].includes(ext)) return res.send('Unsupported filetype');
        newFilename = `${Date.now()}_${file.md5}.${ext}`;
        mvPath = 'images/' + newFilename;
        await file.mv(mvPath);
    }

    let ip = req.ip;
    if(config.cloudflare){
        ip = req.headers['cf-connecting-ip'];
    }
    if(bans.includes(ip)){
        if(mvPath) await fs.promises.rm(mvPath);
        return res.send('You are banned!');
    }

    const postID = await createPost(
        req.body.parent,
        config.admins.includes(ip) ? null : ip,
        req.body.x == '' ? 0 : req.body.x,
        req.body.y == '' ? 0 : req.body.y,
        req.body.topic,
        req.body.username,
        req.body.content,
        originalFilename,
        newFilename
    );
    if(postID < 0){
        if(mvPath) await fs.promises.rm(mvPath);
        switch(postID){
            case -1:
                return res.send('Wait 1 minute between posts');
            case -2:
                return res.send('You cannot reply to a nonexistent post');
            case -3:
                return res.send('You cannot reply to a reply');
        }
    }

    if(newFilename){
        await createThumbnail(mvPath, ext, postID).catch(async err=>{
            if(mvPath) await fs.promises.rm(mvPath);
            await db.deletePost(postID);
            return res.send('Invalid file format');
        }).then(()=>{
            res.redirect(parentID > 0 ? '/?t=' + parentID : '/?t=' + postID);
        })
    }else{
        res.redirect(parentID > 0 ? '/?t=' + parentID : '/?t=' + postID);
    }
}

async function handleDelete(req, res){
    const ip = req.ip;
    if(!config.admins.includes(ip)){
        res.send('You are not authorized to delete posts');
    }else{
        const selectedThreads = req.path.match(/(?<=^\/delete\/)([0-9,]*)/g)[0];
        const threadIDs = selectedThreads.split(',').map(id=>Number(id));
        await removePosts(threadIDs);
        res.send('Done');
    }
}

(async ()=>{
    db = new CanvasDB('posts.db');
    await db.init();

    await fs.promises.mkdir('thumbs', {recursive: true});
    await fs.promises.mkdir('images', {recursive: true});

    app.use('/thumbs', express.static('thumbs'));
    app.use('/images', express.static('images'));
    app.get('/delete/*', handleDelete);
    app.use('/', express.static('client'));
    app.use(fileUpload({
        limits: {
            fileSize: 2**24
        },
        abortOnLimit: true
    }));
    app.use(cors());
    app.post('/post', handlePost);

    const server = app.listen(config.port);

    server.on('upgrade', (request, socket, head) => {
        wsServer.handleUpgrade(request, socket, head, socket => {
            wsServer.emit('connection', socket, request);
        });
    });
})();
